require 'http'

project_id = "#{ENV['CI_PROJECT_ID']}"
pipeline_id = "#{ENV['CI_PIPELINE_ID']}"

# Fetching bridges associated to the pipeline
url = "https://gitlab.com/api/v4/projects/#{project_id}/pipelines/#{pipeline_id}/bridges"

puts url

response = HTTP.auth("Bearer #{ENV['TOKEN']}").get(url)

puts response.status

return unless response.status == 200

result = JSON.parse(response.body)

bridge = result.select { |job| job['name'] == 'deploy_1' }.first
status = bridge.dig('status')

if status == 'success'
  puts 'deployment success'
else
  puts 'deployment failed'
  raise StandardError, 'deployment failed'
end
